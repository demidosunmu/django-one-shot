from django.urls import path
from todos.views import (
    todo_list_list,
    show_todo_list,
    create_todo_list,
    edit_todo_list,
)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("todos/<int:id>/", show_todo_list, name="todo_list_detail"),
    path("todos/create/", create_todo_list, name="create_todo_list"),
    path("todos/<int:id>/edit/", edit_todo_list, name="todo_list_update"),
]
